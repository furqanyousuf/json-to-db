const merge = require('deepmerge')
const fs = require('fs')
const fsp = fs.promises
const { v4: uuid } = require('uuid')
const logger = require('./config/config.js').logger
const {
  ungzip
} = require('node-gzip')
const mysql = require('mysql')
const dbconfig = require('./config/config').db
const countArrays = process.env.COUNT_ARRAYS || false
const lineSeparator = ' '
var options = {
  dateTimeColumns: process.env.DateTimeColumns ? process.env.DATETIME_COLUMNS.split(',') : ['startedDateTime'],
  ignoredObjects: process.env.IGNORED_OBJECTS ? process.env.IGNORED_OBJECTS.split(',') : ['js'],
  parent: process.env.PARENT,
  synchronize: process.env.SYNCHRONIZE || false,
  nulls: ['{}', '[]', 'null', 'NULL', '""'],
  tablePrefix: process.env.TABLE_PREFIX || '',
  stringLimit: process.env.STRING_LIMIT || 512
}

const synchronizeTable = (tableName, columns, parentColumn, connection, baseArray) => new Promise((resolve, reject) => {
  // console.log(`tableName:${tableName}`)
  // console.log(`columns:${columns}`)
  // console.log(`parentColumn:${parentColumn}`)
  if (!tableName || !options.synchronize || (columns.length === 0 && !parentColumn) || baseArray) { return resolve() }
  let parentTable
  if (parentColumn) { parentTable = options.tablePrefix + parentColumn }

  var createCommand = 'CREATE TABLE IF NOT EXISTS `' + dbconfig.database + '`.`' + tableName + '`('
  if (columns.includes('Oid')) { createCommand += 'Oid BINARY(16),PRIMARY KEY(Oid)' } else if (parentColumn) { createCommand += `\`${parentColumn}\` BINARY(16),FOREIGN KEY (\`${parentColumn}\`) REFERENCES \`${parentTable}\` (\`Oid\`) ON UPDATE CASCADE ON DELETE CASCADE` } else { createCommand += `${columns[0]} VARCHAR(256)` }
  createCommand += ');'
  // console.log(createCommand)
  connection.query(createCommand, (error, results, fields) => {
    if (error) { return reject(error) }

    var exisitingColumnsCommand = 'SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA=? AND TABLE_NAME=?'
    connection.query(exisitingColumnsCommand, [dbconfig.database, tableName], (e, r, f) => {
      var cols = []
      if (e) { return reject(e) }
      r.forEach(element => {
        cols.push(element.COLUMN_NAME.toLowerCase())
      })
      var filtered = columns.filter((value, index, arr) => {
        return !cols.includes(value.toLowerCase())
      })
      if (filtered.length > 0) {
        var alterTableCommand = `ALTER TABLE \`${dbconfig.database}\`.\`${tableName}\`` + filtered.map(c => 'ADD `' + c + '` ' + (c === parentColumn ? 'BINARY(16)' : (tableName === options.tablePrefix + 'window' && c === 'value' ? 'JSON' : ('VARCHAR(' + (tableName === 'JS_headers' ? 256 : options.stringLimit) + ')')))).join(',')
        if (filtered.includes(parentColumn)) { alterTableCommand += `,ADD FOREIGN KEY (\`${parentColumn}\`) REFERENCES \`${parentTable}\` (\`Oid\`) ON UPDATE CASCADE ON DELETE CASCADE` }
        if (filtered.includes('Oid')) { alterTableCommand += ',ADD PRIMARY KEY(Oid)' }

        connection.query(alterTableCommand, [dbconfig.database, tableName], (ee, rr, ff) => {
          if (ee) {
            logger.error(alterTableCommand)
            return reject(ee)
          }
          return resolve()
        })
      } else {
        return resolve()
      }
    })
  })
})

const insertTable = (tableName, columns, values, parent, connection) => new Promise((resolve, reject) => {
  if (!tableName /* || (parent && columns.filter(d => { return d !== 'Oid' }).length < 2) */) {
    return resolve()
  }
  // console.log(`tableName:${tableName}`)
  var l = values.length - 1
  for (let i = l; i >= 0; i--) {
    var v = values[i]
    if (options.nulls.indexOf(v) > -1) {
      values.splice(i, 1)
      columns.splice(i, 1)
    }
  }

  let qms2 = ''
  for (var i = 0; i < columns.length; i++) {
    if (options.dateTimeColumns.indexOf(columns[i]) >= 0) {
      qms2 += "STR_TO_DATE(?,'%Y-%m-%dT%T'),"
      values[i] = values[i].substr(0, 19)
    } else {
      qms2 += '?,'
    }
  }
  qms2 = qms2.substr(0, qms2.length - 1)
  if (tableName === `${options.tablePrefix}window` && columns.includes('value')) {
    // qms2 = '?,CAST(? as JSON),?'
    // values[1] = JSON.stringify(values[1])
  }
  var ss = 'INSERT INTO ' + lineSeparator + '`' + dbconfig.database + '`.`' + tableName + '`' + lineSeparator + '(`' + columns.join('`,`') + '`)' + lineSeparator + 'VALUES' + lineSeparator + '(' + qms2 + ');'
  // if (tableName === 'har_extraHeaders') {
  //   console.log({ ss: ss })
  //   console.log({ values: values })
  // }
  connection.query(ss, values, (ee, rr, ff) => {
    if (ee && ee.code !== 'ER_JSON_DOCUMENT_TOO_DEEP') {
      if (ee.code === 'ER_DATA_TOO_LONG') { 
        console.warn(tableName)
        console.warn(ee.sqlMessage.split("'")[1]) 
      }
      return reject(ee)
    }
    return resolve(true)
  })
})
const simplePromise = () => new Promise((resolve, reject) => resolve())
const beginTransaction = (connection) => new Promise((resolve, reject) => connection.beginTransaction((err) => err ? reject(err) : resolve()
))
const commitTransaction = (connection) => new Promise((resolve, reject) => connection.commit((err) => err ? reject(err) : resolve()))
const rollbackTransaction = (connection) => new Promise((resolve, reject) => connection.rollback((err) => err ? reject(err) : resolve()))

const processJSON = (connection, data, currentObject, parent, parentUuid) => {
  return new Promise((resolve, reject) => {
    var i = 0
    var tableName
    if (currentObject) { tableName = options.tablePrefix + currentObject }
    // if (tableName === 'js_window') { console.log(data) }
    var columns = []
    var values = []
    const oid = Buffer.alloc(16)
    uuid(null, oid, 0)
    var cd = []
    let co = false
    if (Array.isArray(data) && data.length > 0) {
      for (const key in data) {
        // childSqlPromises.push(processJSON(data[key], currentObject, parent, parentUuid));
        // if (typeof data[key] === 'object') { co = true }
        cd.push({
          con: connection,
          d: data[key],
          c: currentObject,
          p: parent,
          u: parentUuid
        })
      }
    } else {
      if (typeof data === 'object') {
        for (const key in data) {
          const piece = data[key]
          const pieceType = typeof piece
          const isBuffer = Buffer.isBuffer(piece)
          const isDate = piece && typeof piece.getMonth === 'function'
          const isArray = Array.isArray(piece)

          if (isArray && countArrays) {
            columns.push(`${key}_Count`)
            values.push(piece.length)
          }

          // console.log({ piece: piece })
          // console.log({ pieceType: pieceType })
          // if (piece) {
          //   console.log('ddddddddddd')
          // }
          if (piece && !isDate && !isBuffer && pieceType === 'object' && !options.ignoredObjects.includes(currentObject)) {
            co = true
            cd.push({
              con: connection,
              d: piece,
              c: key,
              p: currentObject,
              u: oid
            })
          } else {
            if (!piece || (piece && ((piece.length === undefined && JSON.stringify(piece).length <= options.stringLimit) || piece.length <= options.stringLimit))) {
              columns.push(key)
              values.push(!options.ignoredObjects.includes(currentObject) || key === 'name' ? piece : JSON.stringify(piece))
            }
          }
          i = i + 1
        }
      } else {
        // logger.warn(currentObject)
        if ((typeof data === 'string' && data.length < options.stringLimit) || typeof data !== 'string') {
          columns.push(currentObject)
          values.push(data)
        }
      }
    }
    if (parent) {
      columns.push(parent)
      values.push(parentUuid)
    }
    if (cd.length > 0 && co) {
    // if ((typeof data === 'object' && !Array.isArray(data)) || (cd.length > 0 && co)) {
      columns.push('Oid')
      values.push(oid)
    }
    synchronizeTable(tableName, columns, parent, connection, Array.isArray(data))
      .then(_ => { return Array.isArray(data) ? simplePromise() : insertTable(tableName, columns, values, parent, connection) })
      .then(_ => cd.reduce((p, x) => p.then(_ => processJSON(x.con, x.d, x.c, x.p, x.u)), Promise.resolve()).catch(e => reject(e)))
      .then(d => resolve())
      .catch(error => reject(error))
  })
}
const processJSONData = (json, ops) => new Promise((resolve, reject) => {
  options = merge(options, ops || {})
  const connection = mysql.createConnection(dbconfig)
  connection.connect()
  let parentObject
  if (options.parent) {
    const spl = options.parent.split('.')
    let index = 0
    do {
      json = json[spl[index]]
    } while (++index < spl.length)
    parentObject = spl[index - 1]
  } else {
    parentObject = 'json'
  }
  beginTransaction(connection)
    .then(_ => processJSON(connection, json, parentObject))
    .then(_ => commitTransaction(connection))
    .then(_ => { connection.end(); return resolve() })
    .catch(error => {
      rollbackTransaction(connection)
      connection.end()
      return reject(error)
    })
    .finally(_ => {})
})
const processFile = (file, ops) => new Promise((resolve, reject) => {
  fsp.readFile(file)
    .then(data => processJSONData(JSON.parse(data.toString('utf8')), ops))
    .then(_ => resolve(_))
    .catch(error => reject(error))
})
const processGzFile = (gzfile, ops) => new Promise((resolve, reject) => {
  fsp.readFile(gzfile)
    .then(data => ungzip(data))
    .then(data => processJSONData(JSON.parse(data.toString('utf8')), ops))
    .then(_ => resolve(_))
    .catch(error => reject(error))
})

module.exports = { processJSONData, processFile, processGzFile }
