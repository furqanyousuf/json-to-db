const winston = require('winston')
const fs = require('fs')

const db = {
  host: process.env.DB_HOST || 'db',
  user: process.env.DB_USER,
  password: process.env.DB_PASSWORD || fs.readFileSync('/run/secrets/' + process.env.DB_PASSWORD_FILE, 'utf8'),
  port: process.env.DB_PORT || 3306,
  database: process.env.DB_NAME,
  multipleStatements: true,
  charset: 'UTF8MB4',
  stringifyObjects: true
}
const appRoot = ''
const bitmodule = 'json-to-db'
var options = {
  info: {
    level: 'info',
    filename: `${appRoot}/logs/${bitmodule}/info.log`,
    handleExceptions: true,
    json: true,
    maxsize: 5242880, // 5MB
    maxFiles: 5,
    colorize: false
  },
  debug: {
    level: 'debug',
    filename: `${appRoot}/logs/${bitmodule}/debug.log`,
    handleExceptions: true,
    json: true,
    maxsize: 5242880, // 5MB
    maxFiles: 5,
    colorize: false
  },
  error: {
    level: 'error',
    filename: `${appRoot}/logs/${bitmodule}/error.log`,
    handleExceptions: true,
    json: true,
    maxsize: 5242880, // 5MB
    maxFiles: 5,
    colorize: false
  },
  warn: {
    level: 'warn',
    filename: `${appRoot}/logs/${bitmodule}/warn.log`,
    handleExceptions: true,
    json: true,
    maxsize: 5242880, // 5MB
    maxFiles: 5,
    colorize: false
  },
  console: {
    level: 'warn',
    handleExceptions: true,
    // json: false,
    colorize: true
  }
}

// eslint-disable-next-line new-cap
const logger = new winston.createLogger({
  transports: [
    new winston.transports.File(options.info),
    new winston.transports.File(options.debug),
    new winston.transports.File(options.error),
    new winston.transports.File(options.warn),
    new winston.transports.Console(options.console)
  ]
  // exitOnError: false, // do not exit on handled exceptions
})

module.exports = {
  db, logger
}
