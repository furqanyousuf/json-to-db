# json-to-mysql
Easily convert your json file into mysql database tables. Features supported are
1. Table Creation
2. Table Alteration/Synchronization for Columns
3. Foreign Keys
4. UUID support for Primary and Foreign Keys
5. Arrays supported
6. Special Count column for arrays
7. Specify start import from specific object
## Installation
```
npm i --save json-to-db
```
## Required Parameters
These can be passed as environment variables or be placed in a .env file and make use of dotenv package to load them.
- SYNCHRONIZE=true/false (whether to update the database tables, little slower since it checks schema before every insert)
- PARENT=`glossary.GlossDiv` (Feature# 7)
- DB_NAME=`db name`
- DB_HOST=`db host`
- DB_PORT=`db port`
- DB_USER=`user`
- DB_PASSWORD=`password`
## Sample Usage
```
const j = require('json-to-db')
require('dotenv').config()
j.processFile('file.json')
```
# Few Samples
## file2.json
````
{
  "glossary": {
    "title": "example glossary",
    "GlossDiv": {
      "title": "S",
      "GlossList": {
        "GlossEntry": {
          "ID": "SGML",
          "SortAs": "SGML",
          "GlossTerm": "Standard Generalized Markup Language",
          "Acronym": "SGML",
          "Abbrev": "ISO 8879:1986",
          "GlossDef": {
            "para": "A meta-markup language, used to create markup languages such as DocBook.",
            "GlossSeeAlso": ["GML", "XML"]
          },
          "GlossSee": "markup"
        }
      }
    }
  }
}
````
###### Generated Schema
![Add](https://res.cloudinary.com/kurkuree/image/upload/v1608663233/vq9ettgs3tf258rhnkt9.png)
## file5.json
```
{
  "menu": {
    "id": "file",
    "value": "File",
    "popup": {
      "menuitem": [{
          "value": "New",
          "onclick": "CreateNewDoc()"
        }, {
          "value": "Open",
          "onclick": "OpenDoc()"
        }, {
          "value": "Close",
          "onclick": "CloseDoc()"
        }
      ]
    }
  }
}
```
###### Generated Schema
![Add](https://res.cloudinary.com/kurkuree/image/upload/v1608739115/file5_mwaxwx.png)